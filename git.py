from tkinter import *
import datetime
import time
import tkinter.colorchooser as cs

class App:

    def __init__(self, master):
        self.colorc='black'
        self.paintcolor='black'
        frame = Frame(master)
        frame.grid(row=1)
        Label(frame, text='时：').grid(row=0, column=0, padx=5)
        e = StringVar()
        self.e1 = Entry(frame, textvariable=e, validate="key", validatecommand=(testCMD, '%p'))
        e.set('0')
        self.e1["width"] = 5
        self.e1.grid(row=0, column=1)
        Label(frame, text='分：').grid(row=0, column=2, padx=5)
        ee1 = StringVar()
        self.e2= Entry(frame, textvariable=ee1, validate="key", validatecommand=(testCMD, '%p'))
        ee1.set('0')
        self.e2["width"] = 5
        self.e2.grid(row=0, column=3)
        Label(frame, text='秒：').grid(row=0, column=4, padx=5)
        ee2 = StringVar()
        self.e3 = Entry(frame, textvariable=ee2, validate="key", validatecommand=(testCMD, '%p'))
        ee2.set('0')
        self.e3["width"] = 5
        self.e3.grid(row=0, column=5)
        frame1 = Frame(master)
        frame1.grid(row=2)
        self.hi_there = Button(frame1, text="Start", width=10, command=self.start)
        self.hi_there.grid(row=0, column=0, padx=10, pady=5)
        self.button = Button(frame1, text="QUIT", width=10, command=frame.quit)
        self.button.grid(row=0, column=1, padx=10, pady=5)
        self.menubar = Menu(master)
        self.fm = Menu(self.menubar, tearoff=False)
        self.fm.add_command(label='画图', command=self.paintpic)
        self.fm.add_separator()
        self.fm.add_command(label='清空', command=lambda x=ALL: self.w.delete(x))
        self.menubar.add_cascade(label='功能', menu=self.fm)
        master.config(menu=self.menubar)

    def start(self):
        t1 = self.e1.get().strip()
        t2 = self.e2.get().strip()
        t3 = self.e3.get().strip()
        h1 = int(t1)
        m1 = int(t2)
        s1 = int(t3)
        self.countDown(h1, m1, s1)

    def countDown(self, h1, m1, s1):
        lbl1.config(bg=self.colorc)
        lbl1.config(height=3, font=('times', 20, 'bold'), fg='white')
        for k1 in range(h1, -1, -1):
            for k2 in range(m1, -1, -1):
                for k3 in range(s1, -1, -1):
                    lbl1["text"] = k1, ':', k2, ':', k3
                    root.update()
                    time.sleep(1)
                if k2 > 0:
                    s1 = 59
            if k1 > 0 and k2 == 0:
                m1 = 59
                s1 = 59
        lbl1.config(bg=self.colorc)
        lbl1.config(fg='gray')
        lbl1["text"] = "计时结束!"

    def call_color_back(self):
        filename = cs.askcolor()
        self.paintcolor = filename[1]

    def paint(self, event):
        x1, y1 = (event.x - 1), (event.y - 1)
        x2, y2 = (event.x + 1), (event.y + 1)
        self.ww.create_line(x1, y1, x2, y2, fill=self.paintcolor)

    def paintpic(self):
        top = Toplevel()
        top.title('画图')
        framenew = Frame(top)
        framenew.grid(sticky=W)
        button = Button(framenew, text="clear", width=8, command=lambda x=ALL: self.ww.delete(x))
        button.grid(row=0, column=0, padx=2, pady=2)
        button2 = Button(framenew, text="color", width=8, fg="red", command=self.call_color_back)
        button2.grid(row=0, column=1, pady=2)
        w = Canvas(top, width=400, height=200, bg='white')
        self.ww = w
        w.grid()
        w.bind("<B1-Motion>", self.paint)
        Label(top, text='按住鼠标左键拖动......').grid()

root = Tk()
def test(content):
    if content.isdigit():
        return True
    else:
        return False
testCMD = root.register(test)
root.title(str(datetime.date.today()))
lbl1 = Label()
lbl1.grid(sticky=N+S+E+W)
app = App(root)
root.mainloop()
